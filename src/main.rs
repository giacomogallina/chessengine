extern crate chess;
use std::io;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::time::{SystemTime};
//use chess::{Board, MoveGen, Piece, Color, EMPTY, ChessMove};
use chess::{Board, Color, BitBoard, Piece, ChessMove};


//struct EvaluatedBoard {
    //board: Board,
    //value: f32,
    ////parent: usize,
    ////children: Vec<usize>,
//}

#[derive(Clone)]
struct SortableMove {
    chess_move: ChessMove,
    value: i32,
}

//impl EvaluatedBoard {
    //fn new(board: Board, parent: usize) -> EvaluatedBoard {
        //EvaluatedBoard {
            //board: board,
            //value: 0.0,
            //parent: parent,
            //children: Vec::with_capacity(50),
        //}
    //}
//}


static mut EP: u64 = 0;
static mut PRUNING_STATS: [u64; 256] = [0; 256];


fn next_move(pos: String) -> String {
    let b = Board::from_fen(pos).unwrap();

    unsafe {
        EP = 0;
    }

    let start_time = SystemTime::now();

    //let mut boards = Vector::with_capacity(10000000);

    //boards.push(EvaluatedBoard::new(b, 0))

    //let moves = MoveGen::new(b, true);
    let mut next_move = None;

    let depth: u32 = 8;

    //if b.side_to_move() == Color::Black {
        //let mut best_value = 1000000.0;

        //for m in moves {
            //let m_value = evaluate_board(&b.make_move(m), depth - 1, best_value);
            //if m_value < best_value {
                //next_move = Some(m);
                //best_value = m_value;
            //}
        //}
    //} else {
        //let mut best_value = -1000000.0;

        //for m in moves {
            //let m_value = evaluate_board(&b.make_move(m), depth - 1, best_value);
            //if m_value > best_value {
                //next_move = Some(m);
                //best_value = m_value;
            //}
        //}
    //}

    let mut moves = [ChessMove::default(); 256];
    let mut sorted_moves: Vec<SortableMove> = vec![SortableMove { 
                                                        chess_move: ChessMove::default(),
                                                        value: 0,
    }; 256*depth as usize];

    let moves_count = b.enumerate_moves(&mut moves);

    for i in 0..moves_count {
        sorted_moves[i].chess_move = moves[i];
    }

    sort_moves(&b, &mut sorted_moves[0..moves_count], 0, depth);

    let mut best_value = i32::max_value();
    for i in 0..moves_count {
        let m_value = evaluate_board_r(&b.make_move(sorted_moves[i].chess_move),
            1, depth, best_value, moves, &mut sorted_moves);
        if m_value < best_value {
            next_move = Some(sorted_moves[i].chess_move);
            best_value = m_value;
        }
    }

    let time_used = start_time.elapsed().unwrap();

    unsafe{
        println!("leafs evaluated: {}", EP);
        print!("pruning stats:");
        for i in 0..32 {
            print!("  {}: {}", i, PRUNING_STATS[i]);
        }
        println!();
    }

    println!("time used: {}.{}", time_used.as_secs(), time_used.subsec_micros());
    println!("depth: {}", depth);

    if let Some(m) = next_move {
        let mut move_str = format!("{}", m);
        move_str.remove(2);
        move_str = move_str.replace("=", "").to_lowercase();
        return move_str;
    }
    String::new()
}

const RANKS: [u64; 8] = [
    255,
    65280,
    16711680,
    4278190080,
    1095216660480,
    280375465082880,
    71776119061217280,
    18374686479671623680,
];

// estimates how good a board is for the player that has to move
// without investigating the next possible situations
fn value(b: &Board) -> i32 {
    let current_player = b.color_combined(b.side_to_move());
    let other_player = b.color_combined(!b.side_to_move());

    let mut value = 
    //let value = 
        64*((b.pieces(Piece::Pawn) & current_player).popcnt() as i32
        - (b.pieces(Piece::Pawn) & other_player).popcnt() as i32)
        
        + 192*((b.pieces(Piece::Knight) & current_player).popcnt() as i32
           + (b.pieces(Piece::Bishop) & current_player).popcnt() as i32
           - (b.pieces(Piece::Knight) & other_player).popcnt() as i32
           - (b.pieces(Piece::Bishop) & other_player).popcnt() as i32)

        + 320*((b.pieces(Piece::Rook) & current_player).popcnt() as i32
           - (b.pieces(Piece::Rook) & other_player).popcnt() as i32)

        + 576*((b.pieces(Piece::Queen) & current_player).popcnt() as i32
           - (b.pieces(Piece::Queen) & other_player).popcnt() as i32);

    if b.side_to_move() == Color::White {
        value += 64*((b.pieces(Piece::Pawn) & BitBoard::new(RANKS[6])) .popcnt() as i32);
        value += 48*((b.pieces(Piece::Pawn) & BitBoard::new(RANKS[5]) & current_player) .popcnt() as i32);
        value -= 64*((b.pieces(Piece::Pawn) & BitBoard::new(RANKS[1])) .popcnt() as i32);
        value -= 48*((b.pieces(Piece::Pawn) & BitBoard::new(RANKS[2]) & other_player) .popcnt() as i32);
    } else {
        value += 64*((b.pieces(Piece::Pawn) & BitBoard::new(RANKS[1])) .popcnt() as i32);
        value += 48*((b.pieces(Piece::Pawn) & BitBoard::new(RANKS[2]) & current_player) .popcnt() as i32);
        value -= 64*((b.pieces(Piece::Pawn) & BitBoard::new(RANKS[6])) .popcnt() as i32);
        value -= 48*((b.pieces(Piece::Pawn) & BitBoard::new(RANKS[5]) & other_player) .popcnt() as i32);
    }
    
    unsafe {
        EP += 1;
    }

    value
}

fn sort_moves(b: &Board, moves: &mut [SortableMove], depth: u32, target_depth: u32) {
    for m in moves.iter_mut() {
        //m.value = match b.piece_on(m.chess_move.get_dest()) {
            //None => -20,
            //Some(p) => {
                //let mut value = match p {
                    //Piece::Pawn => 1,
                    //Piece::Bishop => 3,
                    //Piece::Knight => 3,
                    //Piece::Rook => 5,
                    //Piece::Queen => 9,
                    //_ => 0,
                //};
                ////match b.piece_on(m.chess_move.get_source()) {
                    ////None => (),
                    ////Some(p) => {
                        ////value -= match p {
                            ////Piece::Pawn => 1,
                            ////Piece::Bishop => 3,
                            ////Piece::Knight => 3,
                            ////Piece::Rook => 5,
                            ////Piece::Queen => 9,
                            ////_ => 0,
                        ////};
                    ////},
                ////}
                //value
            //}
        //}

        m.value = -value(&b.make_move(m.chess_move));

        

        //m.value = -evaluate_board_r(&b.make_move(m.chess_move), 0, 1, \c 

        //evaluate_move(b.make_move(m), 0, depth_left - 
    }
    moves.sort_unstable_by_key(|m| -m.value);
}

fn evaluate_board_r(b: &Board, depth: u32, target_depth: u32, alpha: i32, 
                    mut moves: [ChessMove; 256], sorted_moves: &mut Vec<SortableMove>) -> i32 {
    if depth == target_depth {
        value(b)
    } else {
        let moves_count = b.enumerate_moves(&mut moves);
        let slice_start = 256*(depth as usize);

        for i in 0..moves_count {
            sorted_moves[slice_start+i].chess_move = moves[i];
        }

        if depth < target_depth-1 {
            sort_moves(b, &mut sorted_moves[slice_start..slice_start+moves_count], depth, target_depth);
        }

        let mut best_value = i32::max_value();
        let mut m_value;
        for i in slice_start..slice_start + moves_count {
            m_value = evaluate_board_r(&b.make_move(sorted_moves[i].chess_move), depth+1,
            target_depth, best_value, moves, sorted_moves); 

            if m_value < best_value {
                best_value = m_value;
                if -m_value >= alpha {
                    if depth < target_depth - 1 {
                        unsafe {
                            PRUNING_STATS[i-slice_start] += 1;
                        }
                    }
                    break;
                }
            }
        }

        - best_value
    }
}

//fn evaluate_board(b: &Board, level: i32, alpha: f32) -> f32 {
    //if level == 0 {
        //let mut value = 0.0;

        ////let mut value = (
            ////(b.pieces(Piece::Pawn) & b.color_combined(Color::White)).popcnt()
            ////- (b.pieces(Piece::Pawn) & b.color_combined(Color::Black)).popcnt()
            
            ////+ 3*((b.pieces(Piece::Knight) & b.color_combined(Color::White)).popcnt()
               ////+ (b.pieces(Piece::Bishop) & b.color_combined(Color::White)).popcnt()
               ////- (b.pieces(Piece::Knight) & b.color_combined(Color::Black)).popcnt()
               ////- (b.pieces(Piece::Bishop) & b.color_combined(Color::Black)).popcnt())

            ////+ 5*((b.pieces(Piece::Rook) & b.color_combined(Color::White)).popcnt()
               ////- (b.pieces(Piece::Rook) & b.color_combined(Color::Black)).popcnt())

            ////+ 9*((b.pieces(Piece::Queen) & b.color_combined(Color::White)).popcnt()
               ////- (b.pieces(Piece::Queen) & b.color_combined(Color::Black)).popcnt())
        ////) as f32;

        //value += (b.pieces(Piece::Pawn) & b.color_combined(Color::White)).popcnt() as f32;
        //value += 3.0*(b.pieces(Piece::Knight) & b.color_combined(Color::White)).popcnt() as f32;
        //value += 3.0*(b.pieces(Piece::Bishop) & b.color_combined(Color::White)).popcnt() as f32;
        //value += 5.0*(b.pieces(Piece::Rook) & b.color_combined(Color::White)).popcnt() as f32;
        //value += 9.0*(b.pieces(Piece::Queen) & b.color_combined(Color::White)).popcnt() as f32;

        //value -= (b.pieces(Piece::Pawn) & b.color_combined(Color::Black)).popcnt() as f32;
        //value -= 3.0*(b.pieces(Piece::Knight) & b.color_combined(Color::Black)).popcnt() as f32;
        //value -= 3.0*(b.pieces(Piece::Bishop) & b.color_combined(Color::Black)).popcnt() as f32;
        //value -= 5.0*(b.pieces(Piece::Rook) & b.color_combined(Color::Black)).popcnt() as f32;
        //value -= 9.0*(b.pieces(Piece::Queen) & b.color_combined(Color::Black)).popcnt() as f32;
        
        //unsafe {
            //EP += 1;
        //}

        //value

        ////value(b)
    //}
    //else {
        //let mut moves = MoveGen::new(*b, true);
        //let targets = b.color_combined(!b.side_to_move());
        //moves.set_iterator_mask(targets);
        //let mut broke = false;

        
        
        //if b.side_to_move() == Color::Black {
            //let mut best_value = 10000000.0;
            //for m in &mut moves {
                //let m_value = evaluate_board(&b.make_move(m), level -1, best_value);
                //if m_value < best_value {
                    //best_value = m_value;
                    //if best_value < alpha {
                        //broke = true;
                        //break;
                    //}
                //}
            //}
            //if !broke {
            ////if !broke && level > 1 {
                //moves.set_iterator_mask(!EMPTY);
                //for m in &mut moves {
                    //let m_value = evaluate_board(&b.make_move(m), level -1, best_value);
                    //if m_value < best_value {
                        //best_value = m_value;
                        //if best_value < alpha {
                            //break;
                        //}
                    //}
                //}
            //}
            //return best_value;
        //} else {
            //let mut best_value = -10000000.0;
            //for m in &mut moves {
                //let m_value = evaluate_board(&b.make_move(m), level -1, best_value);
                //if m_value > best_value {
                    //best_value = m_value;
                    //if best_value > alpha {
                        //broke = true;
                        //break;
                    //}
                //}
            //}
            //if !broke {
            ////if !broke && level > 1 {
                //moves.set_iterator_mask(!EMPTY);
                //for m in &mut moves {
                    //let m_value = evaluate_board(&b.make_move(m), level -1, best_value);
                    //if m_value > best_value {
                        //best_value = m_value;
                        //if best_value > alpha {
                            //break;
                        //}
                    //}
                //}
            //}
            //return best_value;
        //}
    //}
//}





fn main() {
    let mut line;
    //let mut file = File::open("output.txt").unwrap();
    let mut f = OpenOptions::new()
        .write(true)
        .append(true)
        //.truncate(true)
        .create(true)
        .open("output.txt")
        .expect("cannot create new blank save file");

    f.write(b"===New Game===\n").unwrap();

    let mut bestmove = "d7d5".to_string();

    loop {
        line = String::new();
        io::stdin().read_line(&mut line)
            .expect("Failed to read line");
        let command: Vec<&str> = line.split_whitespace().collect();
        if command.len() > 0 {
            if command[0] == "uci" {
                f.write_all(b"received uci command!\n").unwrap();
                println!("id name SuperMatch");
                println!("id author gg");
                println!("uciok");
            }
            else if command[0] == "isready" {
                f.write_all(b"received isready command!\n").unwrap();
                println!("readyok");
            }
            else if command[0] == "go" {
                f.write_all(b"received go command!\n").unwrap();
                println!("bestmove {}", bestmove);
                //println!("bestmove {}", moves[current_move]);
                //current_move += 1;
            }
            else if command[0] == "position" {
                let pos: String = command.iter()
                    .skip(3)
                    .fold(command[2].to_string(), |acc, x| format!("{} {}", acc, x));
                f.write_all(b"received position command!\n").unwrap();
                //println!("{}", pos);
                bestmove = next_move(pos);
            }

        }
        
        //f.write_all(command[0].as_bytes()).unwrap();
        f.write_all(line.as_bytes()).unwrap();
    }
}
